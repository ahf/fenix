/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull

import org.mozilla.fenix.BuildConfig

import org.torproject.android.service.OrbotConstants
import org.torproject.android.service.TorService
import org.torproject.android.service.TorServiceConstants
import org.torproject.android.service.util.TorServiceUtils

interface TorEvents {
    fun onTorConnecting()
    fun onTorConnected()
    fun onTorStatusUpdate(entry: String?, status: String?)
    fun onTorStopped()
}

class TorController(
    private val context: Context
) : TorEvents {

    private val lbm: LocalBroadcastManager = LocalBroadcastManager.getInstance(context)
    private val entries =  mutableListOf<Pair<String?,String?>>()
    val logEntries get() = entries

    private var torListeners = mutableListOf<TorEvents>()

    private var pendingRegisterChangeList = mutableListOf<Pair<TorEvents, Boolean>>()
    private var lockTorListenersMutation = false

    private var isTorStarted = false
    private var isTorBootstrapped = false

    val isDebugLoggingEnabled get() =
        context.
        getSharedPreferences("org.torproject.android_preferences", Context.MODE_PRIVATE).
        getBoolean("pref_enable_logging", false)

    val isBootstrapped get() = isTorBootstrapped

    fun start() {
        // Register receiver
        lbm.registerReceiver(
            localBroadcastReceiver,
            IntentFilter(TorServiceConstants.ACTION_STATUS)
        )
        lbm.registerReceiver(
            localBroadcastReceiver,
            IntentFilter(TorServiceConstants.LOCAL_ACTION_LOG)
        )
    }

    fun stop() {
        lbm.unregisterReceiver(localBroadcastReceiver)
    }

    private val localBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action ?: return
            if (action != TorServiceConstants.ACTION_STATUS &&
                action != TorServiceConstants.LOCAL_ACTION_LOG) {
                return
            }

            val logentry: String?
            val status: String?
            if (action == TorServiceConstants.LOCAL_ACTION_LOG) {
                logentry = intent.getExtras()
                    ?.getCharSequence(TorServiceConstants.LOCAL_EXTRA_LOG) as? String?
            } else {
                logentry = null
            }

            status = intent.getExtras()
                ?.getCharSequence(TorServiceConstants.EXTRA_STATUS) as? String?

            if (logentry == null && status == null) {
                return
            }

            entries.add(Pair(logentry, status))

            onTorStatusUpdate(logentry, status)
            if (status != null && status.startsWith("ON")) {
                isTorStarted = true
            }

            if (logentry != null && logentry.contains(TorServiceConstants.TOR_CONTROL_PORT_MSG_BOOTSTRAP_DONE)) {
                isTorBootstrapped = true
                onTorConnected()
            }

            if (isTorStarted && status != null && status.startsWith("OFF")) {
                setTorStopped()
            }
        }
    }

    override fun onTorConnecting() {
        lockTorListenersMutation = true
        torListeners.forEach{it.onTorConnecting()}
        lockTorListenersMutation = false

        handlePendingRegistrationChanges()
    }

    override fun onTorConnected() {
        lockTorListenersMutation = true
        torListeners.forEach{it.onTorConnected()}
        lockTorListenersMutation = false

        handlePendingRegistrationChanges()
    }

    override fun onTorStatusUpdate(entry: String?, status: String?) {
        lockTorListenersMutation = true
        torListeners.forEach{it.onTorStatusUpdate(entry, status)}
        lockTorListenersMutation = false

        handlePendingRegistrationChanges()
    }

    override fun onTorStopped() {
        lockTorListenersMutation = true
        torListeners.forEach{it.onTorStopped()}
        lockTorListenersMutation = false

        handlePendingRegistrationChanges()
    }

    fun registerTorListener(l: TorEvents) {
        if (torListeners.contains(l)) {
            return
        }

        if (lockTorListenersMutation) {
            pendingRegisterChangeList.add(Pair(l, true))
        } else {
            torListeners.add(l)
        }
    }

    fun unregisterTorListener(l: TorEvents) {
        if (!torListeners.contains(l)) {
            return
        }

        if (lockTorListenersMutation) {
            pendingRegisterChangeList.add(Pair(l, false))
        } else {
            torListeners.remove(l)
        }
    }

    private fun handlePendingRegistrationChanges() {
        pendingRegisterChangeList.forEach {
            if (it.second) {
                registerTorListener(it.first)
            } else {
                unregisterTorListener(it.first)
            }
        }

        pendingRegisterChangeList.clear()
    }

    /**
     * Receive the current Tor status.
     *
     * Send a request for the current status and receive the response.
     * Returns true if Tor is running, false otherwise.
     *
     */
    private suspend fun checkTorIsStarted(): Boolean {
        val channel = Channel<Boolean>()

        // Register receiver
        val lbm: LocalBroadcastManager = LocalBroadcastManager.getInstance(context)
        val localBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action ?: return
                // We only want ACTION_STATUS messages
                if (action != TorServiceConstants.ACTION_STATUS) {
                    return
                }
                // The current status has the EXTRA_STATUS key
                val currentStatus =
                    intent.getStringExtra(TorServiceConstants.EXTRA_STATUS)
                channel.offer(currentStatus === TorServiceConstants.STATUS_ON)
            }
        }
        lbm.registerReceiver(
            localBroadcastReceiver,
            IntentFilter(TorServiceConstants.ACTION_STATUS)
        )

        // Request service status
        val torServiceStatus = Intent(context, TorService::class.java)
        torServiceStatus.action = TorServiceConstants.ACTION_STATUS
        context.startService(torServiceStatus)

        // Wait for response and unregister receiver
        var torIsStarted = false
        withTimeoutOrNull(torServiceResponseTimeout) {
            torIsStarted = channel.receive()
        }
        lbm.unregisterReceiver(localBroadcastReceiver)
        return torIsStarted
    }

    fun initiateTorBootstrap(lifecycleScope: LifecycleCoroutineScope, withDebugLogging: Boolean = false) {
        if (BuildConfig.DISABLE_TOR) {
            return
        }

        context.getSharedPreferences("org.torproject.android_preferences", Context.MODE_PRIVATE)
            .edit().putBoolean("pref_enable_logging", withDebugLogging).apply()

        lifecycleScope.launch {
            val torNeedsStart = !checkTorIsStarted()
            if (torNeedsStart) {
                val torServiceStatus = Intent(context, TorService::class.java)
                torServiceStatus.action = TorServiceConstants.ACTION_START
                context.startService(torServiceStatus)
            }
        }
    }

  fun stopTor() {
      if (BuildConfig.DISABLE_TOR) {
          return
      }

      val torService = Intent(context, TorService::class.java)
      context.stopService(torService)
  }

  fun setTorStopped() {
      isTorStarted = false
      isTorBootstrapped = false
      onTorStopped()
  }


    companion object {
        const val torServiceResponseTimeout = 5000L
    }
}
