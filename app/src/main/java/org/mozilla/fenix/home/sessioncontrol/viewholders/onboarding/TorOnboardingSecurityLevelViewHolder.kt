/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders.onboarding

import android.view.View
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tor_onboarding_security_level.view.*
import org.mozilla.fenix.R
import org.mozilla.fenix.ext.components
import org.mozilla.fenix.ext.settings
import org.mozilla.fenix.home.sessioncontrol.OnboardingInteractor
import org.mozilla.fenix.onboarding.OnboardingRadioButton
import org.mozilla.fenix.utils.view.addToRadioGroup

class TorOnboardingSecurityLevelViewHolder(
    view: View,
    private val interactor: OnboardingInteractor
) : RecyclerView.ViewHolder(view) {

    private var standardSecurityLevel: OnboardingRadioButton
    private var saferSecurityLevel: OnboardingRadioButton
    private var safestSecurityLevel: OnboardingRadioButton

    init {
        view.header_text.setOnboardingIcon(R.drawable.ic_onboarding_tracking_protection)

        standardSecurityLevel = view.security_level_standard_default
        saferSecurityLevel = view.security_level_safer_option
        safestSecurityLevel = view.security_level_safest_option

        view.description_text.text = view.context.getString(
            R.string.tor_onboarding_security_level_description
        )

        view.open_settings_button.setOnClickListener {
            interactor.onOpenSettingsClicked()
        }

        setupRadioGroup()
    }

    private fun setupRadioGroup() {

        addToRadioGroup(standardSecurityLevel, saferSecurityLevel, safestSecurityLevel)

        standardSecurityLevel.isChecked = true
        safestSecurityLevel.isChecked = false
        saferSecurityLevel.isChecked = false

        standardSecurityLevel.onClickListener {
            updateSecurityLevel()
        }

        saferSecurityLevel.onClickListener {
            updateSecurityLevel()
        }

        safestSecurityLevel.onClickListener {
            updateSecurityLevel()
        }
    }

    private fun updateSecurityLevel() {
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_onboarding_security_level
    }
}
