/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders

import android.text.method.MovementMethod
import android.text.method.ScrollingMovementMethod
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tor_bootstrap_logger.view.*
import org.mozilla.fenix.R
import org.mozilla.fenix.tor.TorEvents
import org.mozilla.fenix.components.Components
import org.mozilla.fenix.home.sessioncontrol.TorBootstrapInteractor

import org.torproject.android.service.TorServiceConstants

class TorBootstrapLoggerViewHolder(
    private val view: View,
    private val components: Components
) : RecyclerView.ViewHolder(view), TorEvents {

    private var entries = mutableListOf<String>()

    init {
        components.torController.registerTorListener(this);

        val currentEntries = components.torController.logEntries.
                filter { it.first != null }.
                filter { !(it.first!!.startsWith("Circuit") && it.second == "ON") }.
                // Keep synchronized with format in onTorStatusUpdate
                flatMap { listOf("(${it.second}) '${it.first}'") }
        val entries_len = currentEntries.size
        val subListOffset = if (entries_len > 24) 24 else entries_len
        entries = currentEntries.subList((entries_len - subListOffset), entries_len) as MutableList<String>
        val initLog = "---------------" + view.resources.getString(R.string.tor_initializing_log) + "---------------"
        entries.add(0, initLog)

        with(view.tor_bootstrap_log_entries) {
            movementMethod = ScrollingMovementMethod()
            text = formatLogEntries(entries)
        }
    }

    private fun formatLogEntries(entries: List<String>) = entries.joinToString("\n")

    override fun onTorConnecting() {
    }

    override fun onTorConnected() {
        components.torController.unregisterTorListener(this)
    }

    override fun onTorStopped() {
    }

    override fun onTorStatusUpdate(entry: String?, status: String?) {
        if (status == null || entry == null) return
        if (status == "ON" && entry.startsWith("Circuit")) return

        if (entries.size > 25) {
            entries = entries.drop(1) as MutableList<String>
        }
        entries.add("(${status}) '${entry}'")

        view.tor_bootstrap_log_entries.text = formatLogEntries(entries)
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_bootstrap_logger
    }
}
