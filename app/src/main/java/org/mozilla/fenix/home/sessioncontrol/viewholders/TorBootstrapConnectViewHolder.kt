/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders

import android.view.View
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tor_bootstrap_connect.view.*
import org.mozilla.fenix.R
import org.mozilla.fenix.tor.TorEvents
import org.mozilla.fenix.tor.bootstrap.TorQuickStart
import org.mozilla.fenix.components.Components
import org.mozilla.fenix.home.sessioncontrol.TorBootstrapInteractor
import org.mozilla.fenix.tor.TorController

import org.torproject.android.service.TorServiceConstants

class TorBootstrapConnectViewHolder(
    private val view: View,
    private val components: Components,
    private val interactor: TorBootstrapInteractor
) : RecyclerView.ViewHolder(view), TorEvents {

    init {
        val torQuickStart = TorQuickStart(view.context)
        setQuickStartDescription(view, torQuickStart)

        with(view.quick_start_toggle as SwitchCompat) {
            setOnCheckedChangeListener { _, isChecked ->
                torQuickStart.setQuickStartTor(isChecked)
                setQuickStartDescription(view, torQuickStart)
            }

            isChecked = torQuickStart.quickStartTor()
        }

        with(view.tor_bootstrap_connect_button) {
            setOnClickListener {
                interactor.onTorBootstrapConnectClicked()
                interactor.onTorStartBootstrapping()

                text = context.resources.getString(R.string.tor_bootstrap_connecting)
            }
        }

        components.torController.registerTorListener(this);
    }

    private fun setQuickStartDescription(view: View, torQuickStart: TorQuickStart) {
        val resources = view.context.resources
        val appName = resources.getString(R.string.app_name)
        if (torQuickStart.quickStartTor()) {
            view.tor_bootstrap_quick_start_description.text = resources.getString(
                R.string.tor_bootstrap_quick_start_enabled, appName
            )
        } else {
            view.tor_bootstrap_quick_start_description.text = resources.getString(
                R.string.tor_bootstrap_quick_start_disabled
            )
        }
    }

    override fun onTorConnecting() {
    }

    override fun onTorConnected() {
        components.torController.unregisterTorListener(this);
    }

    override fun onTorStopped() {
    }

    override fun onTorStatusUpdate(entry: String?, status: String?) {
        if (entry == null) return

        if (status != null && status.startsWith("STOPPING") && entry.contains("Unable to start Tor")) {
            // Only restart Tor with Debug logging once
            if (components.torController.isDebugLoggingEnabled) {
                with(view.tor_bootstrap_connect_button) {
                    text = context.resources.getString(R.string.tor_bootstrap_connect)
                    isEnabled = false
                }
                return
            }

            interactor.onTorStartDebugBootstrapping()
        }

        view.tor_bootstrap_status_message.text = entry
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_bootstrap_connect
    }
}
